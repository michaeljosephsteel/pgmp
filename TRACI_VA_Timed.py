import os
import sys
import traci
import numpy as np

gui_f=True


if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")

exe = 'sumo-gui.exe' if gui_f else 'sumo.exe'
sumoBinary = os.path.join(os.environ['SUMO_HOME'], 'bin', exe)
sumoCmd = [sumoBinary, "--start", '-c' "map.sumocfg"]
print(sumoCmd)

# 012345
# ONOWEO 
traci.start(sumoCmd)
North   = "OGOrrO"
North_y = "OyOrrO"
East    = "OrOrGO"
East_y  = "OrOryO"
West    = "OrOGrO"
West_y  = "OrOyrO"
Red     = "OrOrrO"


step = 0
while step < 43200:
    traci.simulationStep()
    step = traci.simulation.getTime()
    State = traci.trafficlight.getRedYellowGreenState("tl_RW")

    if traci.inductionloop.getLastStepVehicleNumber("TL_N") > 0:
        if North == State:
            traci.trafficlight.setRedYellowGreenState("tl_RW", North)
        else:
            traci.trafficlight.setRedYellowGreenState("tl_RW", North_y)
            for number in range(15):
                traci.simulationStep()
            traci.trafficlight.setRedYellowGreenState("tl_RW", North)
    for number in range(60):
                traci.simulationStep()
    if traci.inductionloop.getLastStepVehicleNumber("TL_E") > 0:
        if East == State:
            traci.trafficlight.setRedYellowGreenState("tl_RW", East)
        else:
            traci.trafficlight.setRedYellowGreenState("tl_RW", East_y)
            for number in range(15):
                traci.simulationStep()
            traci.trafficlight.setRedYellowGreenState("tl_RW", East)
    for number in range(60):
                traci.simulationStep()
    if traci.inductionloop.getLastStepVehicleNumber("TL_W") > 0:
        if West == State:
            traci.trafficlight.setRedYellowGreenState("tl_RW", West)
        else:
            traci.trafficlight.setRedYellowGreenState("tl_RW", West_y)
            for number in range(15):
                traci.simulationStep()
            traci.trafficlight.setRedYellowGreenState("tl_RW", West)
    for number in range(60):
                traci.simulationStep()


traci.close()