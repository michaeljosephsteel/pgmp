# PGMP
Autonomous Temporary Traffic Control Using Embedded Machine Learning

## SUMO

A copy of SUMO is required for this program to function correctly.<br>
Due to the mechanics used the Nightly branch is required available in several formats at the following link

https://sumo.dlr.de/docs/Downloads.php#nightly_snapshots

## Test Files 

The following files are given as additional proof of work but may not function and are not intended to.<br>
They demonstrate a workflow and process for where the code was heading.

[TRACI_Test.py](https://gitlab.com/michaeljosephsteel/pgmp/-/blob/master/TRACI_Test.py)<br>
[TRACI_Test_RL.py](https://gitlab.com/michaeljosephsteel/pgmp/-/blob/master/TRACI_Test_RL.py)
