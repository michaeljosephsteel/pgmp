import os
import sys
import traci
import numpy as np

gui_f=True


if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")

exe = 'sumo-gui.exe' if gui_f else 'sumo.exe'
sumoBinary = os.path.join(os.environ['SUMO_HOME'], 'bin', exe)
sumoCmd = [sumoBinary, "--start", '-c' "map.sumocfg"]
print(sumoCmd)

# 012345
# ONOWEO 
traci.start(sumoCmd)
North   = "OGOrrO"
North_y = "OyOrrO"
East    = "OrOrGO"
East_y  = "OrOryO"
West    = "OrOGrO"
West_y  = "OrOyrO"
Red     = "OrOrrO"
OFF     = "OOOOOO"

step = 0
while step < 43200:
    traci.simulationStep()
    step = traci.simulation.getTime()
    State = traci.trafficlight.getRedYellowGreenState("tl_RW")



    traci.trafficlight.setRedYellowGreenState("tl_RW", OFF)

traci.close()